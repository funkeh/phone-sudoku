gen = { }


function gen.grid()

    math.randomseed(os.time())

    for i = 1, 9 do

        local origin = math.random(3)

        local cell1, cell2 = { }, { }

        local dir = 'h'
        if (math.random() >= 0.5) then
            dir = 'v'
        end

        cell1 = { (origin - 1) * 3 + math.random(3), math.random(9) }
        ::retryGen::
        dir = 'v'
        cell2 = { (origin - 1) * 3 + math.random(3), math.random(9) }
        if (cell2[1] == cell1[1] or
                math.floor((cell2[2] - 1) / 3) == math.floor((cell1[2] - 1) / 3)) then
            goto retryGen
        end

        if dir == 'h' then
            numbersFixed[cell1[1]][cell1[2]] = i
            numbersFixed[cell2[1]][cell2[2]] = i
        else
            numbersFixed[cell1[2]][cell1[1]] = i
            numbersFixed[cell2[2]][cell2[1]] = i
        end

    end

end
