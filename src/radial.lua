-- chikun :: 2014
-- Sudoku radial menu

return {

    x = 0,
    y = 0,
    update = function(dt)

        selection.hover = 0

        if selection.held > 0 and selection.acti then
            if math.dist(radial.x, radial.y, love.mouse.getX(), love.mouse.getY()) > (scale * 6) then

                -- Get angle
                local angle = math.deg(math.angle(radial.x, radial.y,
                    love.mouse.getX(), love.mouse.getY()))

                -- Convert angle to only positive numbers
                if angle < 0 then angle = angle + 360 end

                -- Add offset to angle
                angle = angle + 110

                -- Take chunk out
                if angle >= 360 then angle = angle - 360 end

                selection.hover = math.ceil(angle / 40)

            end
        end

    end,
    draw = function()

        -- Draw radial menu around mouse
        local radMenu = {
            x = radial.x,
            y = radial.y
        }

        if selection.held > 0 then

            local radius = selection.held * 2

            -- If selection is modifiable then
            if selection.acti then

                if selection.held > 0.5 then
                    radius = 1
                end

                love.graphics.setColor(game.theme.accent[1], game.theme.accent[2],
                    game.theme.accent[3], 192)

            else

                radius = radius * 0.65

                love.graphics.setColor(game.theme.deny[1], game.theme.deny[2],
                    game.theme.deny[3], 192)

            end

            love.graphics.circle('fill', radMenu.x, radMenu.y, scale * 15 * radius, 120)

            love.graphics.setColor(game.theme.bg[1], game.theme.bg[2],
                game.theme.bg[3], 192)

            if selection.hover == 0 then
                love.graphics.circle('fill', radMenu.x, radMenu.y, scale * 6 * radius, 120)
            else
                local tmp = (selection.hover - 1) * 40
                love.graphics.arc('fill', radMenu.x, radMenu.y,
                scale * 15 * radius,
                math.rad(-110 + tmp), math.rad(-70 + tmp), 16)
            end

        end


        if selection.held > 0.5 and selection.acti then

            -- Draw radial numbers
            love.graphics.setFont(fonts.radial)

            for i = 0, 8 do

                local dir = math.rad(i * 360 / 9 - 90)
                local tmpX, tmpY = radMenu.x + math.cos(dir) * scale * 12,
                                   radMenu.y + math.sin(dir) * scale * 12
                local dx = tmpX - scale * 2
                local dy = tmpY - fonts.radial:getHeight() / 2, scale * 4

                local tmpCol = game.theme.bg
                if (i == selection.hover - 1) then
                    tmpCol = game.theme.fg
                end

                love.graphics.setColor(tmpCol[1], tmpCol[2],
                    tmpCol[3], (selection.held - 0.5) * 2 * 255)

                love.graphics.printf(i + 1, dx, dy, scale * 4, 'center')

            end

        end

    end

}
