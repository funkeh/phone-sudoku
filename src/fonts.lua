-- chikun :: 2014
-- Font loader


-- The font file to load
local fontFile = "kingsbridge-rg.ttf"


return {
    title   = love.graphics.newFont(fontFile, scale * 8),
    radial  = love.graphics.newFont(fontFile, scale * 6)
}
