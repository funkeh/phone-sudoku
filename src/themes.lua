-- chikun :: 2014
-- Program themes


-- Return table of all themes
return {

    -- Light theme (default)
    light = {
        accent  = { 156, 39,  176 },
        bg      = { 255, 255, 255 },
        deny    = { 244, 67,  54  },
        fade    = { 64,  64,  64  },
        fg      = { 0,   0,   0   },
        number  = { 0,   150, 136 }
    },


    -- Dark theme
    dark = {
        accent  = { 205, 220, 57  },
        bg      = { 33,  33,  33  },
        deny    = { 244, 67,  54  },
        fade    = { 96,  96,  96  },
        fg      = { 255, 255, 255 },
        number  = { 255, 152, 0   }
    },


    -- Glitch theme
    glitch = {
        accent  = { 255, 255, 255 },
        bg      = { 128, 192, 255 },
        deny    = { 244, 67,  54  },
        fade    = { 128, 32,  96  },
        fg      = { 192, 64,  128 },
        number  = { 0,   150, 136 }
    }

}
