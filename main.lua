-- chikun :: 2014
-- Radical Sudoku


function love.load()

    -- Load maths extensions
    require("src/maths")

    -- Load generation functions
    require("src/generate")

    -- Load themes
    themes = require("src/themes")

    game = {
        width  = love.graphics.getWidth(),
        height = love.graphics.getHeight(),
        theme = themes.light
    }

    scale = game.width / 100

    fonts = require("src/fonts")

    radial = require("src/radial")

    numbersFixed = {
        { 3, 0, 5, 0, 0, 0, 0, 2, 9 },
        { 0, 1, 0, 0, 0, 0, 0, 5, 0 },
        { 0, 4, 0, 9, 0, 0, 8, 0, 0 },
        { 0, 7, 6, 3, 0, 2, 0, 4, 8 },
        { 0, 0, 3, 4, 0, 5, 9, 0, 0 },
        { 4, 9, 0, 7, 0, 6, 2, 3, 0 },
        { 0, 0, 8, 0, 0, 7, 0, 9, 0 },
        { 0, 5, 0, 0, 0, 0, 0, 1, 0 },
        { 2, 3, 0, 0, 0, 0, 7, 0, 6 }
    }

    -- gen.grid()

    numbersPlayer = {
        { 0, 0, 0, 0, 0, 0, 0, 0, 0 },
        { 0, 0, 0, 0, 0, 0, 0, 0, 0 },
        { 0, 0, 0, 0, 0, 0, 0, 0, 0 },
        { 0, 0, 0, 0, 0, 0, 0, 0, 0 },
        { 0, 0, 0, 0, 0, 0, 0, 0, 0 },
        { 0, 0, 0, 0, 0, 0, 0, 0, 0 },
        { 0, 0, 0, 0, 0, 0, 0, 0, 0 },
        { 0, 0, 0, 0, 0, 0, 0, 0, 0 },
        { 0, 0, 0, 0, 0, 0, 0, 0, 0 }
    }

    selection = {
        data = { 0, 0 },
        held = 0,
        dir  = 0,
        acti = false,
        hover = 0
    }


    box = {
        x = scale * 10,
        y = (scale * 17) + (game.height - (scale * 17)) / 2 - scale * 40,
        w = scale * 80,
        h = scale * 80
    }

end


function love.update(dt)

    selection.held = math.clamp(0, selection.held + dt * 8 * selection.dir, 1)

    -- If at
    if selection.held == 1 and not selection.acti then
        selection.dir = -1
    end

    -- Reset selection.dir if radial hidden
    if selection.held == 0 then
        selection.dir = 0
    end

    -- Update radial menu
    radial.update(dt)

end


function love.draw()

    -- Set background colour
    love.graphics.setBackgroundColor(game.theme.bg)

    -- Set font
    love.graphics.setFont(fonts.title)
    love.graphics.setColor(game.theme.accent)
    love.graphics.print("Radical Sudoku", scale * 4, scale * 4)


    -- Draw bar
    love.graphics.setColor(game.theme.fg)
    love.graphics.rectangle("fill", 0, scale * 16, scale * 100, scale)

    local centre = {
        x = scale * 50,
        y = (scale * 17) + (game.height - (scale * 17)) / 2
    }


    local size, space = 40, 80/9

    -- Draw faded lines
    love.graphics.setColor(game.theme.fade)

    for x = 0, 9 do
        love.graphics.line(centre.x - scale * (size - space * x), centre.y - scale * size,
            centre.x - scale * (size - space * x), centre.y + scale * size)
    end

    for y = 0, 9 do
        love.graphics.line(centre.x - scale * size, centre.y - scale * (size - space * y),
            centre.x + scale * size, centre.y - scale * (size - space * y))
    end


    -- Draw hard lines
    love.graphics.setColor(game.theme.fg)

    for x = 0, 9, 3 do
        love.graphics.line(centre.x - scale * (size - space * x), centre.y - scale * size,
            centre.x - scale * (size - space * x), centre.y + scale * size)
    end

    for y = 0, 9, 3 do
        love.graphics.line(centre.x - scale * size, centre.y - scale * (size - space * y),
            centre.x + scale * size, centre.y - scale * (size - space * y))
    end


    -- Draw numbers in table
    for x = 0, 8 do
        for y = 0, 8 do

            love.graphics.setColor(game.theme.fg)

            local num = numbersFixed[y+1][x+1]

            if num == 0 then

                love.graphics.setColor(game.theme.number)

                num = numbersPlayer[y+1][x+1]

            end

            if num ~= 0 then

                love.graphics.printf(num,
                    centre.x - scale * (size - space * x),
                    centre.y - scale * (size - 0.25 - space * y), scale * space, 'center')

            end

        end
    end


    -- Draw radial menu
    radial.draw()


end


function love.mousepressed(x, y, mb)

    ratio = (80 / 9) * scale

    dx = math.floor((x - box.x) / ratio)
    dy = math.floor((y - box.y) / ratio)

    if x >= box.x and y >= box.y and x < box.x + box.w and y < box.y + box.h then

        selection.held = 0

        radial.x = dx * ratio + box.x + ratio / 2
        radial.y = dy * ratio + box.y + ratio / 2

        selection.acti = (numbersFixed[dy+1][dx+1] == 0)

        selection.dir = 1

    end

    if y < scale * 17 then

        if game.theme == themes.light then
            game.theme = themes.dark
        else
            game.theme = themes.light
        end

    end

end


function love.mousereleased()

    -- If actually selecting something, unselect
    if selection.acti then

        selection.dir = -1


        if selection.hover > 0 then

            dx = math.floor((radial.x - box.x) / ratio)
            dy = math.floor((radial.y - box.y) / ratio)

            numbersPlayer[dy+1][dx+1] = selection.hover

        end

    end

end
