-- chikun :: 2014
-- Phone Sudoku configuration


function love.conf(context)

    context.console = true

    context.window.title = "Phone Sudoku"

    context.window.width = 240
    context.window.height = 320

    context.window.fsaa = 4

end
